package com.jitender.controller;

import com.jitender.query.AbstrEmployeeFldFactory;
import com.jitender.query.EmployeeFldProducer;
import com.jitender.vo.Employee;
import com.jitender.vo.Filter;
import com.jitender.vo.Filters;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.*;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import  com.jitender.query.EmployeeFldProducer.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by jitender on 10/23/2016.
 */

public class FilterUtil {
    @Autowired
    EmployeeFldProducer employeeFldProducer;
    List<Employee> loadEmpList() {
        List<Employee> empList = new ArrayList<>();

        for (int i = 1; i <= 100000; i++) {
            Employee e = new Employee();
            e.dateHired = LocalDate.now().minusDays(i);
            e.departmentName = "dept" + String.valueOf(i);
            e.employeeId = i;
            e.firstName = "first" + String.valueOf(i);
            e.lastName = "last" + String.valueOf(i);
            e.managerId = i + 1000;
            e.lastPasswordChange = LocalDateTime.now().minusMinutes(i);
            empList.add(e);
        }

        return empList;
    }


    public List<Employee> filter(List<Employee> employees, List<Filters> filters) {
        if (employees == null || employees.isEmpty()) {
            throw new RuntimeException("employee list  not loaded");
        }
        if (filters == null || filters.isEmpty()) {
            return employees;
        }
        Stream<Employee> empStream = employees.parallelStream();
        List <Predicate<Employee>> pradicateList = new ArrayList<>();
        for (Filters f : filters) {
                //functional to get rid of if labels ?
            AbstrEmployeeFldFactory employeeFldFactory=null;

            if (f.getEmployeeId()!=null &&f.getEmployeeId().equalsIgnoreCase("entityTypeId")) {
                 employeeFldFactory = employeeFldProducer.getFactory("employeeId");
              Predicate p =  employeeFldFactory.getemployeeId( Integer.parseInt(f.getValue()),  f.getOperator());
                pradicateList.add(p); //hack to prevent steam from cloning

            }else  if (f.getManagerId()!=null && f.getManagerId().equalsIgnoreCase("entityTypeId")) {
                 employeeFldFactory =employeeFldProducer.getFactory("managerId");
                Predicate p =  employeeFldFactory.getManagerId( Integer.parseInt(f.getValue()),  f.getOperator());
                pradicateList.add(p);
            }else  if (f.getDepartmentName()!=null && f.getDepartmentName().equalsIgnoreCase("entityTypeId")) {
                 employeeFldFactory = employeeFldProducer.getFactory("departmentName");
                Predicate p =  employeeFldFactory.departmentName(f.getValue(),  f.getOperator());
                pradicateList.add(p);
            }else  if (f.getFirstName()!=null && f.getFirstName().equalsIgnoreCase("entityTypeId")) {
                 employeeFldFactory = employeeFldProducer.getFactory("firstName");
                Predicate p =  employeeFldFactory.firstName( f.getValue(),  f.getOperator());
                pradicateList.add(p);
            }else  if (f.getLastName()!=null && f.getLastName().equalsIgnoreCase("entityTypeId")) {
                 employeeFldFactory = employeeFldProducer.getFactory("lastName");
                Predicate p =  employeeFldFactory.lastName( f.getValue(),  f.getOperator());
                pradicateList.add(p);
            }else if (f.getJobDescription()!=null&& f.getJobDescription().equalsIgnoreCase("entityTypeId")) {
                 employeeFldFactory = employeeFldProducer.getFactory("jobDescription");
                Predicate p =  employeeFldFactory.jobDescription( f.getValue(),  f.getOperator());
                pradicateList.add(p);
            }else  if (f.getDateHired()!=null&& f.getDateHired().equalsIgnoreCase("entityTypeId")) {
                 employeeFldFactory = employeeFldProducer.getFactory("dateHired");
                Predicate p =  employeeFldFactory.dateHired( f.getValue(),  f.getOperator());
                pradicateList.add(p);
            }else  if (f.getLastPasswordChange()!=null&& f.getLastPasswordChange().equalsIgnoreCase("entityTypeId")) {
                 employeeFldFactory = employeeFldProducer.getFactory("lastPasswordChange");
                Predicate p =  employeeFldFactory.lastPasswordChange( f.getValue(),  f.getOperator());
                pradicateList.add(p);
            }

        }

        return empStream.filter(emp -> {
            Stream<Predicate<Employee>> predicateStream = pradicateList.stream();
            return predicateStream.map(predicate -> predicate.test(emp)).reduce((a, b) -> a && b).get();
        }).collect(Collectors.toList()); // employees.stream().filter(()->).collect(Collectors.toList());

    }
}

