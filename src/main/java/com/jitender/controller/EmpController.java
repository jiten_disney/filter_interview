package com.jitender.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.jitender.vo.Employee;
import com.jitender.vo.Filter;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.List;

/**
 * Created by jitender on 10/23/2016.
 */


@RestController
public class EmpController {
    static Log log = LogFactory.getLog(EmpController.class);

    @Autowired
    ObjectMapper objectMapper;

    @Autowired
    FilterUtil filterUtil;

    @RequestMapping(value = "/api/employee",method = RequestMethod.GET)
    public List<Employee> getFilteredEmployee(@RequestParam("filter") String filter) {
    log.debug("param - "+filter);
        try {
            Filter fil =  objectMapper.readValue(filter,Filter.class);
            log.debug("file - "+fil.getFilters().get(0));

            return  filterUtil.filter( filterUtil.loadEmpList() ,fil.getFilters()) ;
        } catch (IOException e) {
            log.error("error - "+e);
            throw new RuntimeException(e);
        }

    }
}
