package com.jitender.vo;

/**
 * Created by jitender on 10/23/2016.
 */
public class Filters {
    private String value;

    private String departmentName;

    public String  employeeId;

    public String firstName;

    public String lastName;

    public String jobDescription;

    public String  managerId;

    public String dateHired;

    public String lastPasswordChange;

    private String operator;

    public String getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(String employeeId) {
        this.employeeId = employeeId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getJobDescription() {
        return jobDescription;
    }

    public void setJobDescription(String jobDescription) {
        this.jobDescription = jobDescription;
    }

    public String getManagerId() {
        return managerId;
    }

    public void setManagerId(String managerId) {
        this.managerId = managerId;
    }

    public String getDateHired() {
        return dateHired;
    }

    public void setDateHired(String dateHired) {
        this.dateHired = dateHired;
    }

    public String getLastPasswordChange() {
        return lastPasswordChange;
    }

    public void setLastPasswordChange(String lastPasswordChange) {
        this.lastPasswordChange = lastPasswordChange;
    }

    public String getValue ()
    {
        return value;
    }

    public void setValue (String value)
    {
        this.value = value;
    }

    public String getDepartmentName ()
    {
        return departmentName;
    }

    public void setDepartmentName (String departmentName)
    {
        this.departmentName = departmentName;
    }

    public String getOperator ()
    {
        return operator;
    }

    public void setOperator (String operator)
    {
        this.operator = operator;
    }



    @Override
    public String toString()
    {
        return "ClassPojo [value = "+value+", departmentName = "+departmentName+", operator = "+operator+"]";
    }
}
