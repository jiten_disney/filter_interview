package com.jitender.vo;

import java.time.LocalDate;
import java.time.LocalDateTime;

/**
 * Created by jitender on 10/23/2016.
 */
public class Employee  {
    public int  employeeId;

    public String firstName;

    public String lastName;

    public String jobDescription;

    public String departmentName;

    public int  managerId;

    public LocalDate dateHired;

    public LocalDateTime lastPasswordChange;

    public int getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(int employeeId) {
        this.employeeId = employeeId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getJobDescription() {
        return jobDescription;
    }

    public void setJobDescription(String jobDescription) {
        this.jobDescription = jobDescription;
    }

    public String getDepartmentName() {
        return departmentName;
    }

    public void setDepartmentName(String departmentName) {
        this.departmentName = departmentName;
    }

    public int getManagerId() {
        return managerId;
    }

    public void setManagerId(int managerId) {
        this.managerId = managerId;
    }

    public LocalDate getDateHired() {
        return dateHired;
    }

    public void setDateHired(LocalDate dateHired) {
        this.dateHired = dateHired;
    }

    public LocalDateTime getLastPasswordChange() {
        return lastPasswordChange;
    }

    public void setLastPasswordChange(LocalDateTime lastPasswordChange) {
        this.lastPasswordChange = lastPasswordChange;
    }

    @Override
    public String toString() {
        return "Employee{" +
                "employeeId=" + employeeId +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", jobDescription='" + jobDescription + '\'' +
                ", departmentName='" + departmentName + '\'' +
                ", managerId=" + managerId +
                ", dateHired=" + dateHired +
                ", lastPasswordChange=" + lastPasswordChange +
                '}';
    }
}
