package com.jitender.vo;

import java.util.List;

/**
 * Created by jitender on 10/23/2016.
 */
public class Filter {
    private List<Filters> filters;

    private String logic;

    public List<Filters> getFilters ()
    {
        return filters;
    }

    public void setFilters (List<Filters> filters)
    {
        this.filters = filters;
    }

    public String getLogic ()
    {
        return logic;
    }

    public void setLogic (String logic)
    {
        this.logic = logic;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [filters = "+filters+", logic = "+logic+"]";
    }
}
