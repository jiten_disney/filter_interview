package com.jitender.query;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created by jitender on 10/24/2016.
 */
@Component
public class EmployeeFldProducer {

    @Autowired
    EmployeeFactory employeeFactory;

    @Autowired
    ManagerFactory managerFactory;

    @Autowired
    FirstNameFactory firstNameFactory;
    @Autowired
    LastNameFactory lastNameFactory;
    @Autowired
    JobDescriptionFactory jobDescriptionFactory;
    @Autowired
    DepartmentNameFactory departmentNameFactory;

    @Autowired
    LastPasswordChangeFactory lastPasswordChangeFactory;

    @Autowired
    DateHiredFactory dateHiredFactory;
    public  AbstrEmployeeFldFactory getFactory(String choice){

        if(choice.equalsIgnoreCase("employeeId")){
            return employeeFactory;

        }else if(choice.equalsIgnoreCase("managerId")){
            return managerFactory;
        }if(choice.equalsIgnoreCase("firstName")){
            return firstNameFactory;

        }else if(choice.equalsIgnoreCase("lastName")){
            return lastNameFactory;
        }if(choice.equalsIgnoreCase("jobDescription")){
            return jobDescriptionFactory;

        }else if(choice.equalsIgnoreCase("departmentName")){
            return departmentNameFactory;
        }else if(choice.equalsIgnoreCase("dateHired")){
            return dateHiredFactory;
        }else if(choice.equalsIgnoreCase("lastPasswordChange")){
            return lastPasswordChangeFactory;
        }



        return null;
    }
}
