package com.jitender.query;

import com.jitender.vo.Employee;
import org.springframework.stereotype.Component;

import java.util.function.Predicate;

/**
 * Created by jitender on 10/25/2016.
 */
@Component
public class DepartmentNameFactory extends AbstrEmployeeFldFactory  {


    @Override
    public Predicate<Employee> getemployeeId(int value, String operator) {
        return null;
    }

    @Override
    public Predicate<Employee> firstName(String value, String operator) {
        return null;
    }

    @Override
    public Predicate<Employee> lastName(String value, String operator) {
        return null;
    }

    @Override
    public Predicate<Employee> jobDescription(String value, String operator) {
        return null;
    }

    @Override
    public Predicate<Employee> departmentName(String value, String operator) {

        if(operator.equalsIgnoreCase("start")){
            return p -> p.getDepartmentName().startsWith(value) ;
        }if(operator.equalsIgnoreCase("end")){
            return p -> p.getDepartmentName().endsWith(value);
        }

        if(operator.equalsIgnoreCase("eq")){
            return p -> p.getDepartmentName().equals(value);
        }
         if(operator.equalsIgnoreCase("neq")){
            return p -> !p.getDepartmentName().equals(value);
        }
        if(operator.equalsIgnoreCase("contains")){
            return p -> p.getDepartmentName().contains(value);
        }
        if(operator.equalsIgnoreCase("ncontains")){
            return p -> !p.getDepartmentName().contains(value);
        }

        return null;
    }

    @Override
    public Predicate<Employee> getManagerId(int value, String operator) {
        return null;
    }

    @Override
    public Predicate<Employee> dateHired(String value, String operator) {
        return null;
    }

    @Override
    public Predicate<Employee> lastPasswordChange(String value, String operator) {
        return null;
    }
}
