package com.jitender.query;

import com.jitender.vo.Employee;
import org.springframework.stereotype.Component;

import java.util.function.Predicate;

/**
 * Created by jitender on 10/24/2016.
 */
@Component
public class FirstNameFactory extends AbstrEmployeeFldFactory  {

    @Override
    public Predicate<Employee> getemployeeId(int value, String operator) {
        return null;
    }

    @Override
    public Predicate<Employee> firstName(String value, String operator) {

            if(operator.equalsIgnoreCase("start")){
                return p -> p.getFirstName().startsWith(value) ;
            }if(operator.equalsIgnoreCase("end")){
                return p -> p.getFirstName().endsWith(value);
            }
            if(operator.equalsIgnoreCase("eq")){
                return p -> p.getFirstName().equals(value);
            }
            if(operator.equalsIgnoreCase("neq")){
                return p -> !p.getFirstName().equals(value);
            }
            if(operator.equalsIgnoreCase("contains")){
                return p -> p.getFirstName().contains(value);
            }
            if(operator.equalsIgnoreCase("ncontains")){
                return p -> !p.getFirstName().contains(value);
          
        }
        return null;
    }

    @Override
    public Predicate<Employee> lastName(String value, String operator) {
        return null;
    }

    @Override
    public Predicate<Employee> jobDescription(String value, String operator) {
        return null;
    }

    @Override
    public Predicate<Employee> departmentName(String value, String operator) {
        return null;
    }

    @Override
    public Predicate<Employee> getManagerId(int value, String operator) {
        return null;
    }

    @Override
    public Predicate<Employee> dateHired(String value, String operator) {
        return null;
    }

    @Override
    public Predicate<Employee> lastPasswordChange(String value, String operator) {
        return null;
    }
}
