package com.jitender.query;

import com.jitender.vo.Employee;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.function.Predicate;

/**
 * Created by jitender on 10/25/2016.
 */
@Component
public class DateHiredFactory extends AbstrEmployeeFldFactory  {

    @Autowired
    DateTimeFormatter formatter;
    @Override
    public Predicate<Employee> getemployeeId(int value, String operator) {
        return null;
    }

    @Override
    public Predicate<Employee> firstName(String value, String operator) {
        return null;
    }

    @Override
    public Predicate<Employee> lastName(String value, String operator) {
        return null;
    }

    @Override
    public Predicate<Employee> jobDescription(String value, String operator) {
        return null;
    }

    @Override
    public Predicate<Employee> departmentName(String value, String operator) {
        return null;
    }

    @Override
    public Predicate<Employee> getManagerId(int value, String operator) {
        return null;
    }

    /*
    *  equal,
    not_equal,
    after,
    before,
    beforequalto,
    afterqualto;*/

    @Override
    public Predicate<Employee> dateHired(String value, String operator) {
        if(operator.equalsIgnoreCase("gt")){
            return p -> p.getDateHired().isAfter(LocalDate.parse(value, formatter))  ;
        }
        if(operator.equalsIgnoreCase("eq")){
            return p -> p.getDateHired().equals(LocalDate.parse(value, formatter))  ;
        }
        if(operator.equalsIgnoreCase("neq")){
            return p -> !p.getDateHired().equals(LocalDate.parse(value, formatter))  ;
        } if(operator.equalsIgnoreCase("lt")){
            return p -> p.getDateHired().isBefore(LocalDate.parse(value, formatter))  ;
        }
        if(operator.equalsIgnoreCase("lte")){
            return p ->! p.getDateHired().isAfter(LocalDate.parse(value, formatter))  ;
        }
        if(operator.equalsIgnoreCase("gte")){
            return p ->! p.getDateHired().isBefore(LocalDate.parse(value, formatter))  ;
        }

        return null;
    }

    @Override
    public Predicate<Employee> lastPasswordChange(String value, String operator) {
        return null;
    }
}
