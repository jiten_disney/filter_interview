package com.jitender.query;

/**
 * Created by jitender on 10/23/2016.
 */
public enum StringPradicate {
    equal,
    not_equal,
    contains,
    not_contain,
    starts;

}
