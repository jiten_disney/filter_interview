package com.jitender.query;

import com.jitender.vo.Employee;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.function.Predicate;

/**
 * Created by jitender on 10/25/2016.
 */
@Component
public class LastPasswordChangeFactory extends AbstrEmployeeFldFactory  {
    @Autowired
    DateTimeFormatter formatter;
    
    @Override
    public Predicate<Employee> getemployeeId(int value, String operator) {
        return null;
    }

    @Override
    public Predicate<Employee> firstName(String value, String operator) {
        return null;
    }

    @Override
    public Predicate<Employee> lastName(String value, String operator) {
        return null;
    }

    @Override
    public Predicate<Employee> jobDescription(String value, String operator) {
        return null;
    }

    @Override
    public Predicate<Employee> departmentName(String value, String operator) {
        return null;
    }

    @Override
    public Predicate<Employee> getManagerId(int value, String operator) {
        return null;
    }

    @Override
    public Predicate<Employee> dateHired(String value, String operator) {
        return null;
    }

    @Override
    public Predicate<Employee> lastPasswordChange(String value, String operator) {
        if(operator.equalsIgnoreCase("gt")){
            return p -> p.getLastPasswordChange().isAfter(LocalDateTime.parse(value, formatter))  ;
        }
        if(operator.equalsIgnoreCase("eq")){
            return p -> p.getLastPasswordChange().equals(LocalDateTime.parse(value, formatter))  ;
        }
        if(operator.equalsIgnoreCase("neq")){
            return p -> !p.getLastPasswordChange().equals(LocalDateTime.parse(value, formatter))  ;
        } if(operator.equalsIgnoreCase("lt")){
            return p -> p.getLastPasswordChange().isBefore(LocalDateTime.parse(value, formatter))  ;
        }
        if(operator.equalsIgnoreCase("lte")){
            return p ->! p.getLastPasswordChange().isAfter(LocalDateTime.parse(value, formatter))  ;
        }
        if(operator.equalsIgnoreCase("gte")){
            return p ->! p.getLastPasswordChange().isBefore(LocalDateTime.parse(value, formatter))  ;
        }
        return null;
    }
}
