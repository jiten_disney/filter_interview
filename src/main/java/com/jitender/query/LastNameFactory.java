package com.jitender.query;

import com.jitender.vo.Employee;
import org.springframework.stereotype.Component;

import java.util.function.Predicate;

/**
 * Created by jitender on 10/25/2016.
 */
@Component
public class LastNameFactory extends AbstrEmployeeFldFactory  {
    @Override
    public Predicate<Employee> getemployeeId(int value, String operator) {
        return null;
    }

    @Override
    public Predicate<Employee> firstName(String value, String operator) {
        return null;
    }

    @Override
    public Predicate<Employee> lastName(String value, String operator) {

        if(operator.equalsIgnoreCase("start")){
            return p -> p.getLastName().startsWith(value) ;
        }if(operator.equalsIgnoreCase("end")){
            return p -> p.getLastName().endsWith(value);
        }
        if(operator.equalsIgnoreCase("eq")){
            return p -> p.getLastName().equals(value);
        }
        if(operator.equalsIgnoreCase("neq")){
            return p -> !p.getLastName().equals(value);
        }
        if(operator.equalsIgnoreCase("contains")){
            return p -> p.getLastName().contains(value);
        }
        if(operator.equalsIgnoreCase("ncontains")){
            return p -> !p.getLastName().contains(value);

        }
        
        
        return null;
    }

    @Override
    public Predicate<Employee> jobDescription(String value, String operator) {
        return null;
    }

    @Override
    public Predicate<Employee> departmentName(String value, String operator) {
        return null;
    }

    @Override
    public Predicate<Employee> getManagerId(int value, String operator) {
        return null;
    }

    @Override
    public Predicate<Employee> dateHired(String value, String operator) {
        return null;
    }

    @Override
    public Predicate<Employee> lastPasswordChange(String value, String operator) {
        return null;
    }
}
