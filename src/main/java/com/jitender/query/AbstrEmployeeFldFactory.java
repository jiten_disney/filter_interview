package com.jitender.query;

import com.jitender.vo.Employee;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.function.Predicate;

/**
 * Created by jitender on 10/24/2016.
 */
public abstract class AbstrEmployeeFldFactory {

    abstract public Predicate<Employee> getemployeeId(int value, String operator);

    abstract  public  Predicate<Employee> firstName(String value, String operator);

    abstract  public  Predicate<Employee> lastName(String value, String operator);

    abstract  public  Predicate<Employee> jobDescription(String value, String operator);

    abstract  public  Predicate<Employee> departmentName(String value, String operator);

    abstract  public Predicate<Employee> getManagerId(int value, String operator);

    abstract  public  Predicate<Employee> dateHired(String value, String operator);

    abstract   public Predicate<Employee> lastPasswordChange(String value, String operator);

}
