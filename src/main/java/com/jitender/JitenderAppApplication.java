package com.jitender;

import com.jitender.controller.FilterUtil;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;
import java.util.Locale;

@SpringBootApplication
public class JitenderAppApplication {

	public static void main(String[] args) {
		SpringApplication.run(JitenderAppApplication.class, args);
	}

	@Bean
	FilterUtil  getFilterUtil(){
        return new FilterUtil();
    }

    @Bean
	DateTimeFormatter getFormatter(){  //8/1/2015 00:00:00

		DateTimeFormatter fmt  =   DateTimeFormatter.ofPattern("M/d/yyyy HH:mm:ss");
		fmt.withLocale(Locale.ENGLISH);
		return fmt;
	}

}

